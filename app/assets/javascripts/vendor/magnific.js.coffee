
# initialise magnific lightbox
$(document).ready ->
  $("#masonry").magnificPopup
    delegate: "a"
    type: "image"
    tLoading: "Loading image #%curr%..."
    mainClass: "mfp-img-mobile" # mfp-with-zoom
    gallery:
      enabled: true
      navigateByImgClick: true
      preload: [ # Will preload 0 - before current, and 1 after the current image
        1
        5
      ]

    # zoom:
    #   enabled: true
    #   duration: 300
    #   opener: (element) ->
    #     element.find "img"

    image:
      verticalFit: true
      tError: "<a href=\"%url%\">The image #%curr%</a> could not be loaded."
      titleSrc: (item) ->
        # item.el.attr("title") + @photo.title

  return
