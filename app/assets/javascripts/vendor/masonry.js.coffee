
jQuery(document).ready ->

  $ ->

    # jQuery Masonry
    masonry = jQuery('#masonry')


    masonry.imagesLoaded ->
      
      masonry.masonry
        itemSelector: '.box'
        isAnimated: !Modernizr.csstransitions

        columnWidth: (containerWidth) ->
          containerWidth / 3

  return
