jQuery(document).ready ->

  # 100% element height for section
  (($) ->
    getHeight = ->
      sH = $(window).height()
      $(".full-height").css "height", sH + "px"
      return
    $(window).load ->
      getHeight()
      return

    $(window).resize ->
      getHeight()
      return

    return
  ) jQuery

  return
