class PhotosController < ApplicationController

  rescue_from Flickr::ApiError, with: :render_not_found

  def index
    @photos = Flickr.photos.search(text: params[:q], sizes: true, sort: 'interestingness-desc', content_type: 1, page: params[:page], per_page: 30)
  end

  def render_not_found
    render nothing: true, status: 404
  end

end
