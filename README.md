# Picturesque

A simple RoR app to showcase some full stack development skills. Check it out here: http://picturesque-staging.herokuapp.com/

## Project summary

All-in-all this app has taken ~6 hours, spread over two days to complete.

* Designed the app using Sketch 3
* Built the app using Slim, SASS, and coffee JS.
* Used the 'flickr-objects' gem for the Flickr API.
* Used 'magnific-popup-rails' for the lightbox.
* and many, many more...

## Notes

* I sacrificed speed in this app so I could have it load the largest available image in the lightbox, which might make this a useful tool for me when searching for large placeholder images for websites.

* The app gets the images related to the search string query, and then orders them by flickr's 'interesting' sorting option, which I assume is some kind of cumulative community engagement weighting.

## To Do

* Tests. Due to limited time I haven't used TDD. I'll try to find some time this weekend, but off-the-top-of-my-head I'll probably use: capybara, rspec, spork, guard, and webmock.

* Infinite scrolling. Most of the code is already written, just haven't got round to finishing it off.

* Speed. Implement lazy loading for next page of images. Abstract out large images from index to stop loading images twice (for list and for lightbox).

## Final Notes

I use this Rails template for all of my projects. It's constantly being updated and feel free to have poke around if you're interested.