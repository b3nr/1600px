source 'https://rubygems.org'
ruby '2.1.1'

### RAILS
### =====
# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.1.4'

### API
### ===
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0',    group: :doc

### SERVER & DB
### ===========
# Use sqlite3 as the database for Active Record
gem 'sqlite3', 					   group: :development
# Use postgres database for Heroku
gem 'pg', 								 group: :production
# Use rails_12factor to enable serving assets on Heroku
gem 'rails_12factor', 		 group: :production
# super fast server
gem 'puma'

### PERFORMANCE
### ===========
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'
# Use JQuery turbolinks to support JQuery masonry
gem 'jquery-turbolinks', '~> 2.0.1'

### FRONT-END (JS)
### ================
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .js.coffee assets and views
gem 'coffee-rails', '~> 4.0.0'
# Use jquery as the JavaScript library
gem 'jquery-rails'
# See https://github.com/sstephenson/execjs#readme for more supported runtimes
gem 'therubyracer',        platforms: :ruby
# Use the JQuery Masonry gem to organise the listings
gem 'masonry-rails'
# Use modernizr to detect native browser features
gem 'modernizr-rails'
# Responsive elements < IE9
gem 'respond-js-rails'

### FRONT-END (CSS)
### =================
# Use SCSS for stylesheets
gem 'sass-rails', '~> 4.0.3'
# Using Bootstrap to make frontend dev super easy
gem 'bootstrap-sass', '~> 3.1.1'
# Add browser vendor prefixes to Bootstrap
gem 'autoprefixer-rails'
# Media queries help manage breakpoints more easily
gem 'sass-mediaqueries-rails'
# Use Slim for nicer templating
gem 'slim'
# Use Bourbon to write way less CSS
gem 'bourbon'

### LIBRARIES
### =========
# Use fontawesome for awesome icons
gem 'font-awesome-rails'
# css animation library
gem 'animate-rails'
# Growl-like flash notifications with JQuery
gem 'growlyflash'

### ENHANCEMENTS
### ============
# Rails Config makes it easier to make application wide changes
gem 'rails_config'
# Use will paginate to add continuous scroll to JQuery Masonry
gem 'will_paginate', '~> 3.0.7'
# Use will paginate bootstrap to work with Bootstrap
gem 'will_paginate-bootstrap'
# Responsive lightbox
gem 'magnific-popup-rails'

### SERVICES
### ========
# Free photos from Flickr
gem 'flickr-objects', '~> 0.5.2'

### BACK-END
### ========
# Figaro manages environment variables
gem 'figaro'

### DEVELOPMENT
### ===========
group :development, :test do
  # Spring speeds up development by keeping your app running in the background
  gem 'spring'
  # Use guard to monitor system files
  gem 'guard'
  # restart server everytime gemfile.lock is modified
  gem 'guard-puma'
  # run migrations if migration files are created or edited
  gem 'guard-migrate', '~> 1.1.0', require: false
  # use livereload plugin for Chrome to not hit the refresh button ever again
  gem 'guard-livereload', require: false
  # Prevent terminal from filling up with asset requests
  gem 'quiet_assets'
  # Stop server and run 'bundle install' when gem file is updated
  gem 'guard-bundler', require: false
  # make the Rails console much friendlier to work with
  gem 'jazz_hands'

  ### TESTING
  ### =======
  # use rspec for BDD (behaviour-driven development)
  # gem 'rspec-rails', '~> 3.0.0'
  # gem 'guard-rspec'
  # use Capybara for ABT (automated browser testing)
  # gem 'capybara'
  # spork makes running tests faster by forking the app beforehand
  # gem 'spork', '~> 1.0rc'
  # gem 'guard-spork'

  ### DEBUGGING
  ### =========
  # Using better errors to help development
  gem 'better_errors'
  # Using Binding of Caller gem to enable variable inspection in better errors gem
  gem 'binding_of_caller'
  # Hooks apps files up to Rails Panel chrome extension
  gem 'meta_request'

  ### QA
  ### ==
  # Use annotate gem to annotate model.rb files with attributes
  gem 'annotate', github: 'ctran/annotate_models'
  # Emails sent by the app open in the browser
  gem 'letter_opener'
end
